<!DOCTYPE html>
<html lang="de">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- favicon -->
    <link rel="shortcut icon" href="./assets/images/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./assets/images/favicon/favicon.ico" type="image/x-icon">

    <!-- Local CSS -->
    <link rel="stylesheet" href="/assets/css/home.css">

    <!-- Google-Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">

    <!-- Frameworks -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="/assets/css/framework/vegas.min.css">

    <title>Marvin Fritz | Student | Programmer</title>
</head>
<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top" id="banner">
        <div class="container">

            <a class="navbar-brand" href="#">Marvin Fritz</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">CLOUD</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Search</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/../app/views/controlpanel/">Login</a>
                    </li>
                    <hr/>
                </ul>
            </div>
        </div>
    </nav>
</header>

<section class="promo vegas-container">
    <div class="vegas-overlay"></div>

    <div class="promo-wrapper vertical-horizontal-center">
        <div class="container">

            <!-- desktop -->
            <div class="d-none d-lg-block">
                <div class="row">
                    <div class="col col-left">
                        <img src="/assets/images/brand.png" class="animated fadeIn delay-2s" width="210" height="210"
                             alt="">
                    </div>
                    <div class="col col-right">
                        <div class="text animated zoomInLeft delay-1s">
                            <span>HI.</span> My name is<br>
                            <h2>Marvin Fritz</h2>
                            I'm a webdesigner and backend developer based in germany.
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile -->
            <div class="d-lg-none">
                <h2>Marvin Fritz</h2>
            </div>

        </div>
    </div>

    <!-- scroll-down icon -->
    <section id="section05" class="scroll-down">
        <a href="#section06"><span></span></a>
    </section>

</section>

<section class="projects">
    <div class="wrapper">
        <div class="title">
            <h4 class="subtitle">WHAT I'VE DONE SO FAR</h4>
            <h2 class="title">PROJECTS</h2>
        </div>
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="card">
                            <div class="image">
                                <img src="./assets/images/home/cards/java.jpg" alt="can't load image">
                            </div>
                            <div class="card-content">
                                <div class="card-title">
                                    <span>JAVA</span>
                                    <h3>Ciera</h3>
                                </div>
                                <div class="desc">
                                    A simple java program to create web-applications based on a template.
                                </div>
                                <div class="more">
                                    <button><a href="#">READ MORE</a></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="card">
                            <div class="image">
                                <img src="./assets/images/home/cards/website.png" alt="can't load image">
                            </div>
                            <div class="card-content">
                                <div class="card-title">
                                    <span>WEBSITE</span>
                                    <h3>marvin-fritz.de</h3>
                                </div>
                                <div class="desc">
                                    Personal website with backend interface
                                </div>
                                <div class="more">
                                    <button><a href="#">READ MORE</a></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="card">
                            <div class="image">
                                <img src="./assets/images/home/cards/wip.png" alt="can't load image">
                            </div>
                            <div class="card-content">
                                <div class="card-title">
                                    <span>-</span>
                                    <h3>Coming soon..</h3>
                                </div>
                                <div class="desc">
                                    Check back later
                                </div>
                                <div class="more">
                                    <button><a href="#">READ MORE</a></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="skills">
    <div class="wrapper">
        <div class="title">
            <h4 class="subtitle">WHAT I'VE LEARNED</h4>
            <h2 class="title">SKILLS</h2>
        </div>

        <div class="content">

        </div>
    </div>
</section>

<section class="contact">
    <div class="wrapper">
        <div class="title">
            <h4 class="subtitle">SAY HI</h4>
            <h2 class="title">GET IN TOUCH</h2>
        </div>

        <div class="content">
            <form action="#">
                <input type="text" name="name" value="Name">
                <br>
                <input type="email" name="email" value="E-Mail">
                <br>
            </form>
        </div>
    </div>
</section>

<footer>
    <div class="content">
        <span>Made with <3 and </> by Marvin Fritz</span><br>
        <span>Copyright © <?php echo date('Y') ?> Marvin Fritz. All rights reserved.</span><br>
        <span><a>Impressum</a> | <a>Datenschutz</a></span>
    </div>
</footer>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<!-- JavaScript -->

<!-- Vegas Plugin -->
<script src="/assets/js/framework/vegas.min.js"></script>
<script id="rendered-js">
    $('.promo').vegas({
        transition: 'zoomOut',
        transitionDuration: 2000,
        delay: 8000,
        timer: true,
        color: '#101113',
        animation: 'random',
        animationDuration: 20000,
        slides: [
            {src: '/assets/images/background6.jpg'},
            {src: '/assets/images/background5.jpg'}
        ],
        overlay: '/assets/images/overlays/06.png'
    });
</script>

<!-- auto resize -->
<script>
    function sizeContent() {
        var newHeight = $("html").height() + "px";
        $(".promo").css("min-height", newHeight);
    }
</script>
<!-- Cookie plugin -->
<script type="text/javascript" src="//s3.amazonaws.com/valao-cloud/cookie-hinweis/script-v2.js"></script>
<script type="text/javascript">
    window.cookieconsent_options = {
        message: 'Diese Website nutzt Cookies, um bestmögliche Funktionalität bieten zu können.',
        dismiss: 'Ok, verstanden',
        learnMore: 'Mehr Infos',
        link: 'https://moritz-drewes.de/privacy',
        theme: 'light-floating'
    };
</script>
</body>
</html>