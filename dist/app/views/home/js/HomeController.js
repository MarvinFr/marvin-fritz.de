/*
    navbar animation
 */

$(document).ready(function () {
    var scroll_pos = 0;



    $(document).scroll(function () {
        scroll_pos = $(this).scrollTop();

        /* Handle scroll down */



        if (scroll_pos > 100) {
            $("nav").css('background-color', 'rgba(37, 37, 37, 0.8)');
            $("nav").css('padding-top', '0px');
            $("header").css('box-shadow', '0px 8px 6px -6px #888');
        } else if (scroll_pos < 100) {
            $("nav").css('background-color', 'transparent');
            $("nav").css('padding-top', '10px');
        }
    });
});