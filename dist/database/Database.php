<?php

namespace Database;

use PDO;

/**
 * Created by PhpStorm.
 * User: marvi
 * Date: 18.09.2018
 * Time: 18:42
 */
class Database
{


    private static $driver = 'mysql';
    private static $host = '127.0.0.1';
    private static $user = 'root';
    private static $pass = '';
    private static $charset = 'utf8';
    private static $database = 'website';

    /*
        private static $driver = 'mysql';
        private static $host = '127.0.0.1';
        private static $user = 'marvin';
        private static $pass = 'Microsoft2711';
        private static $charset = 'utf8';
        private static $database = 'marvin-fritz';
    */
    public function connection()
    {
        $driver = self::$driver;
        $host = self::$host;
        $database = self::$database;
        $charset = self::$charset;

        $pdo = new PDO("{$driver}:host={$host};dbname={$database};charset={$charset}", self::$user, self::$pass);
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        return $pdo;
    }

    public static function query($query, $params) {
        $sth = self::connection()->prepare($query);
        $sth->execute($params);
        $data = $sth->fetchAll();
        return $data;
    }

}