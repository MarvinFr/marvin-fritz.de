<?php

namespace Database\methods;

use Database\Database;

include(dirname(__DIR__) . '/../database/Database.php');

class AccessStatistic
{

    public function __construct()
    {
        $this->registerConnection();
    }

    public function createTable()
    {
        Database::query("CREATE TABLE IF NOT EXISTS connections(id int auto_increment primary key, ip VARCHAR(80) NOT NULL, last_connection VARCHAR(100) NULL, connections INT(10) NOT NULL, browser TEXT NOT NULL, blocked INT DEFAULT 0 NULL)", []);
    }

    public function insert($client_ip, $date, $browser)
    {
        Database::query("INSERT INTO connections(id, ip, last_connection, connections, browser, blocked) VALUES (null, :ip, :date ,'1', :browser, null)", [
            ':ip' => $client_ip,
            ':date' => $date,
            ':browser' => $browser
        ]);
    }

    public function registerConnection()
    {
        $this->createTable();
        if ($this->connectionAlreadyRegistered() == false) {
            $this->insert($this->getClientIP(), $this->getDate(), $this->getBrowser());
            return 0;
        } else {
            $this->addConnection();
            $this->updateLastConnection();
            return 0;
        }
    }

    public function addConnection()
    {
        $client_ip = $this->getClientIP();
        $connections = $this->getConnections() +1;
        Database::query("UPDATE connections SET connections = :connections WHERE ip = :ip", [
            ":connections"   => $connections,
            ":ip" => $client_ip
        ]);
    }

    public function getConnections()
    {
        $client_ip = $this->getClientIP();
        $request = Database::query("select * from connections where ip = :ip", [
            ":ip" => $client_ip
        ]);
        foreach ($request as $row) {
            $connections = $row['connections'];
            return $connections;
        }
        return 0;
    }

    public function connectionAlreadyRegistered()
    {
        $client_ip = $this->getClientIP();
        $result = Database::query("select ip from connections where ip = :ip", [
            ":ip" => $client_ip
        ]);
        if ($result == null) {
            return false;
        } else {
            return true;
        }
    }

    public function getLastConnection()
    {
        $client_ip = $this->getClientIP();
        $request = Database::query("select last_connection from connections where ip = :ip", [
            ":ip" => $client_ip
        ]);
        foreach ($request as $row) {
            $connections = $row['last_connection'];
            return $connections;
        }
        return 0;
    }

    public function updateLastConnection()
    {
        $client_ip = $this->getClientIP();
        Database::query("update connections set last_connection = :connection_date where ip = :ip", [
            ":ip" => $client_ip,
            ":connection_date" => $this->getDate()
        ]);
    }

    /*
     * Utils
     */

    public function getDate()
    {
        $timestamp = time();
        return date("d.m.Y - H:i", $timestamp);
    }

    public function getBrowser()
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    public function getClientIP()
    {
        if (!isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        if ($ip == '::1')
            $ip = 'localhost';

        return $ip;
    }
}