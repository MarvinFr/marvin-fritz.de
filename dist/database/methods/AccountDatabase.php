<?php


namespace Database\methods;


use Database\Database;

class AccountDatabase
{

    public function __construct()
    {
    }

    public function createTable() {
        Database::query('CREATE TABLE IF NOT EXISTS accounts(id int auto_increment primary key, username VARCHAR(32) NOT NULL, email VARCHAR(150) NOT NULL, password VARCHAR NOT NULL, verified INT(1), blocked INT(1))');
    }

}